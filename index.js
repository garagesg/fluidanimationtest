

var deltaTime = 0.0007;
var r = 3;
var h = ( Math.sqrt(2) + 0.01 ) / 128;
var density = 998;
var mass = 998 / Math.pow(128,3);
var K = 90;
var miu = 8;
var force = {x:0,y:0};
var particles = [];

var canvasSize = 800;

var densityFormula1 = (315 * mass) / (64 * Math.PI * Math.pow(h,9));
var pressureFormula1 =  (27.5*mass*K) / ( Math.PI * Math.pow(h,6) ) ;
var viscosityFormula1 = ( 45*mass*miu ) / ( Math.PI*Math.pow(h,6) );
var cohesionFormula1 = 32/(Math.PI*Math.pow(h,9));
var cohesionFormula2 = Math.pow(h,6)/64;
var normFormula = - (945*mass)/(32*Math.PI*Math.pow(h,9));

var segments = 128;
var scalingFactor = canvasSize/segments;

var voxels = [];
var maxParticlesInVoxel = 4;

var obstacles = [];

var container = {
	position:{
		x:0.5,
		y:0.5
	},
	r:0.5
};

var mouse = {
	position:{x:0.6,y:0.8},
	r:0.1
};

var tensionIntensity = 1;

var mouseCircle = document.createElementNS("http://www.w3.org/2000/svg","circle");
var dragMouse = false;

function updateMouse(x,y){
	if(dragMouse){
		mouse.position.x = x/canvasSize;
		mouse.position.y = y/canvasSize;
		mouseCircle.setAttributeNS(null,"cx",mouse.position.x*canvasSize);
		mouseCircle.setAttributeNS(null,"cy",mouse.position.y*canvasSize);

		// console.log(obstacles[0].position,mouse.position)
	}
}

function init(){
	var counter = 0;

    for(var i = 48; i < 88; i ++){
    	for(var j = 48; j < 88; j ++){
        	particles[counter] = new Particle( (i+0.5)/128 , (j+0.5)/128 , counter);
        	counter++;
    	}
    }

	var myCircle = document.createElementNS("http://www.w3.org/2000/svg","circle");
	myCircle.setAttributeNS(null,"cx",canvasSize*0.5);
	myCircle.setAttributeNS(null,"cy",canvasSize*0.5);
	myCircle.setAttributeNS(null,"r",canvasSize*0.5);
	myCircle.setAttributeNS(null,"fill","none");
	myCircle.setAttributeNS(null,"stroke","white");
	myCircle.setAttributeNS(null,"stroke-width","1");
	document.getElementById("mySVG").appendChild(myCircle);

	mouseCircle.setAttributeNS(null,"cx",canvasSize*mouse.position.x);
	mouseCircle.setAttributeNS(null,"cy",canvasSize*mouse.position.y);
	mouseCircle.setAttributeNS(null,"r",canvasSize*0.1);
	mouseCircle.setAttributeNS(null,"fill","none");
	mouseCircle.setAttributeNS(null,"stroke","white");
	mouseCircle.setAttributeNS(null,"stroke-width","1");
	document.getElementById("mySVG").appendChild(mouseCircle);

	obstacles.push(mouse);

	document.onmousedown = function(event){
		dragMouse = true;
	}
	document.onmouseup = function(event){
		dragMouse = false;
	}
	document.onmousemove = function(event){
		updateMouse(event.pageX, event.pageY)
	}

	drawParticles();
}

function getAcceleration(density, pressure, viscosity, gravity, cForce, mForce){
	return (1/density) * (pressure + viscosity) + gravity + cForce + mForce;
}

function placeParticlesInVoxels() {

	//clears and reset voxels
	voxels = [];
    for(var i = 0; i < segments*segments; i ++){
    	voxels[i] = [];
    }

	for( var j = 0 ; j < particles.length; j++){

		var currentVoxel = {
			x:Math.floor(particles[j].position.x*segments), 
			y:Math.floor(particles[j].position.y*segments)
		};
		var index = currentVoxel.x + (segments*currentVoxel.y);
		voxels[index].push(j);

	}

}

function Particle(x,y,count) {

	this.id = count;

	this.position = {
		x: x,
		y: y
	}
	this.oldPosition = {
		x: x,
		y: y
	}
	this.velocity = {
		x: 0,
		y: 0
	}
	this.oldVelocity = {
		x: 0,
		y: 0
	}
	this.acceleration = {
		x: 0,
		y: 0
	}

	this.cohesionForce = {
		x: 0,
		y: 0,
		c: 0
	}

	this.minimizationForce = {
		x: 0,
		y: 0
	}

	this.normals = {
		x: 0,
		y: 0
	}

	this.neighbors = [];
	this.density = density;
	this.getPressureAndViscosity();

	var myCircle = document.createElementNS("http://www.w3.org/2000/svg","circle");
	myCircle.setAttributeNS(null,"cx",this.position.x*canvasSize);
	myCircle.setAttributeNS(null,"cy",this.position.y*canvasSize);
	myCircle.setAttributeNS(null,"r",r);
	myCircle.setAttributeNS(null,"fill","white");
	myCircle.setAttributeNS(null,"stroke","none");
	document.getElementById("mySVG").appendChild(myCircle);

	this.circle = myCircle;
	this.circle._parent = this;

}
Particle.prototype.getNeighbors = function() {
	
	var _this = this;

	this.neighbors = [];

	for( var i = 0 ; i < particles.length; i++){
		
		var dist = distanceBetween(_this.position,particles[i].position);
		if(dist <= h){
			this.neighbors.push(particles[i]);
		}

	}

}
function getDensity(currentPt){

	var summation = 0;

	for(var i = 0;i<currentPt.neighbors.length;i++){
		var neighbor = currentPt.neighbors[i];
		var dist = distanceBetween(neighbor.position,currentPt.position);
		var density = Math.pow( (h*h) - (dist*dist) , 3);
		summation += density;
	}

	return densityFormula1*summation;
}

function getNeighborsAndDensity() {


	for( var i = 0 ; i < particles.length; i++){

		var particle = particles[i];
		particle.neighbors = [];
		var summation = 0;
		var normSum = 0;

		for( var w = -1 ; w<=1; w++ ){
			for ( var u = -1; u<=1; u++){

				var currentVoxel = {
					x:Math.floor(particle.position.x*segments) + w, 
					y:Math.floor(particle.position.y*segments) + u
				};
				var index = currentVoxel.x + (segments*currentVoxel.y);
				//checks if index is out of bounds
				if(index > 0 && index < voxels.length){

					for(var k = 0; k < voxels[index].length; k++){
						if(k < maxParticlesInVoxel){
							var neighbor = particles[voxels[index][k]];
							var dist = distanceBetween(neighbor.position,particle.position);

							if(dist <= h) {
								var density = Math.pow( (h*h) - (dist*dist) , 3);
								particle.neighbors.push(neighbor);
								summation += density;
							}

						}
					}

				}

			}
		}

		particle.density = densityFormula1*summation;

	}


}

function containsObject(obj, list) {
    var i;
    for (i = 0; i < list.length; i++) {
        if (list[i] === obj) {
            return true;
        }
    }

    return false;
}

Particle.prototype.getNormals = function(){
	var normSumX = 0;
	var normSumY = 0;
	var cohesionForce = 0;

	for(var i = 0;i<this.neighbors.length;i++){
		var neighbor = this.neighbors[i];
		var dist = distanceBetween(neighbor.position,this.position);
		if(dist > 0 && neighbor != this){
			//normals
			var same = Math.pow(Math.pow(h,2)-Math.pow(dist,2),2)/this.density;
			normSumX += same * (this.position.x - neighbor.position.x);
			normSumY += same * (this.position.y - neighbor.position.y);
		}
	}
	this.normals = {x:normFormula*normSumX,y:normFormula*normSumY};
}

Particle.prototype.getPressureAndViscosity = function() {
	var pSumX = 0;
	var pSumY = 0;
	var vSumX = 0;
	var vSumY = 0;
	var cSumX = 0;
	var cSumY = 0;
	var mSumX = 0;
	var mSumY = 0;
	var cohesionForce = 0;
	for(var i = 0;i<this.neighbors.length;i++){
		var neighbor = this.neighbors[i];
		var dist = distanceBetween(neighbor.position,this.position);
		if(dist > 0 && neighbor != this){
			var same = ((this.density + neighbor.density - 2*density)/neighbor.density)* Math.pow(h - dist,2) / dist;
			var pressureX =  same * ((this.position.x - neighbor.position.x));
			var pressureY =  same * ((this.position.y - neighbor.position.y));
			pSumX += pressureX;
			pSumY += pressureY;
			var viscosityX = ((neighbor.velocity.x - this.velocity.x)/neighbor.density) * (h - dist);
			var viscosityY = ((neighbor.velocity.y - this.velocity.y)/neighbor.density) * (h - dist);
			vSumX += viscosityX;
			vSumY += viscosityY;

			if(dist>=h*0.5){
				cohesionForce = (2*Math.pow( (h-dist),3 ) * Math.pow(dist,3) ) - cohesionFormula2;
			}else{
				cohesionForce = cohesionFormula1 * Math.pow( (h-dist),3 ) * Math.pow(dist,3);
			}

			this.cohesionForce.c = cohesionForce;

			var cohesionX = tensionIntensity * mass * mass * this.cohesionForce.c * (this.position.x - neighbor.position.x);
			var cohesionY = tensionIntensity * mass * mass * this.cohesionForce.c * (this.position.y - neighbor.position.y);
			cSumX += cohesionX;
			cSumY += cohesionY;
			var mSame = mass*tensionIntensity;
			var mSumX = (this.normals.x - neighbor.normals.x) * mSame;
			var mSumY = (this.normals.y - neighbor.normals.y) * mSame;
			mSumX += mSumX;
			mSumY += mSumY;
		}
	}
	this.pressure = {x:pressureFormula1*pSumX,y:pressureFormula1*pSumY};
	this.viscosity = {x:viscosityFormula1 * vSumX,y:viscosityFormula1 * vSumY};
	this.cohesionForce.x = cSumX;
	this.cohesionForce.y = cSumY;
	this.minimizationForce.x = mSumX;
	this.minimizationForce.y = mSumY;
	if(this.id == 60){
		console.log(this.minimizationForce)
	}

}

Particle.prototype.update = function () {

	// this.density = getDensity(this);
	this.getPressureAndViscosity();

	this.acceleration.x = getAcceleration(this.density, this.pressure.x, this.viscosity.x, force.x, this.cohesionForce.x, this.minimizationForce.x)
	this.acceleration.y = getAcceleration(this.density, this.pressure.y, this.viscosity.y, force.y, this.cohesionForce.y, this.minimizationForce.y)

}

Particle.prototype.draw = function (){

	this.velocity.x = this.velocity.x + this.acceleration.x * deltaTime;
	this.velocity.y = this.velocity.y + this.acceleration.y * deltaTime;
	this.position.x = this.position.x + this.velocity.x * deltaTime;
	this.position.y = this.position.y + this.velocity.y * deltaTime;

	//make sure it's within container
	var normalL = distanceBetween(this.position, container.position); 
	var dist = normalL - container.r;
	if(dist >= 0){
		var n = {};
		n.x = (container.position.x - this.position.x) / normalL;
		n.y = (container.position.y - this.position.y) / normalL;

		this.velocity.x = 0;
		this.velocity.y = 0;
		this.position.x = container.position.x - n.x * container.r;
		this.position.y = container.position.y - n.y * container.r;
	}

	// for(var i = 0; i < obstacles.length ; i++){
		var obstacle = mouse;
		var normalL = distanceBetween(this.position, obstacle.position); 
		var dist = normalL - obstacle.r;
		
		if(dist < 0){
			var n = {};
			n.x = (obstacle.position.x - this.position.x) / normalL;
			n.y = (obstacle.position.y - this.position.y) / normalL;

			this.velocity.x = 0;
			this.velocity.y = 0;
			this.position.x = obstacle.position.x - n.x * obstacle.r;
			this.position.y = obstacle.position.y - n.y * obstacle.r;
		}
	// }

	this.oldVelocity.x = this.velocity.x;
	this.oldVelocity.y = this.velocity.y;
	this.oldPosition.x = this.position.x;
	this.oldPosition.y = this.position.y;

	this.circle.setAttributeNS(null,"cx",this.position.x*canvasSize);
	this.circle.setAttributeNS(null,"cy",this.position.y*canvasSize);

}

function drawParticles(){

	placeParticlesInVoxels();
	getNeighborsAndDensity();

	for(var k = 0 ; k<particles.length;k++){
		var particle = particles[k];
		particle.getNormals();
	}
	for(var i = 0 ; i<particles.length;i++){
		var particle = particles[i];
		particle.update();
	}
	for(var j = 0 ; j<particles.length;j++){
		var particle = particles[j];
		particle.draw();
	}
	
	requestAnimationFrame(drawParticles);
}

window.onload = init;

/* MATH FUNCTIONS */
function distanceBetween(a,b){
	var dx = a.x - b.x;
	var dy = a.y - b.y;
	return Math.sqrt(dx * dx + dy * dy);
}
