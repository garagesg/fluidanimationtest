# README #

This codebase is following lessons from Hector Arellano (https://twitter.com/hector_arellano) given at Firstborn. 

### What is this repository for? ###

* Quick summary
This is a process of fluid simulations using SPH particles. We're going through the process from CPU rendering and moving towards GPU rendering.

### How do I get set up? ###

* Summary of set up
Run `npm install`
Run `npm run start`

* Dependencies
NodeJS
NPM